// jQuery.sap.declare("com.xiaomi.test.mismartchart.Component");
// sap.ui.getCore().loadLibrary("sap.ui.generic.app");
// jQuery.sap.require("sap.ui.generic.app.AppComponent");

// sap.ui.generic.app.AppComponent.extend("com.xiaomi.test.mismartchart.Component", {
// 	metadata: {
// 		"manifest": "json"
// 	}
// });

sap.ui.define([
	'sap/ui/core/UIComponent',
	'sap/ui/fl/FakeLrepConnectorLocalStorage',
	'sap/ui/core/util/MockServer'
], function(UIComponent, FakeLrepConnectorLocalStorage, MockServer) {
	"use strict";

	return UIComponent.extend("com.xiaomi.test.mismartchart.Component", {
		metadata: {
			manifest: "json"
		}
		// ,init: function() {
		// 	FakeLrepConnectorLocalStorage.enableFakeConnector();
		// 	UIComponent.prototype.init.apply(this, arguments);

		// 	//Start Mockserver
		// 	this._oMockServer = new MockServer({
		// 		rootUri: "/MockDataService/"
		// 	});
		// 	var sMockdataUrl = sap.ui.require.toUrl("com/xiaomi/test/mismartchart/mockserver");
		// 	var sMetadataUrl = sMockdataUrl + "/metadata.xml";
		// 	this._oMockServer.simulate(sMetadataUrl, {
		// 		sMockdataBaseUrl: sMockdataUrl,
		// 		aEntitySetsNames: [
		// 			"LineItemsSet", "SEPMRA_C_ALP_SalesOrderType"
		// 		]
		// 	});
		// 	this._oMockServer.start();
		// },
		// destroy: function() {
		// 	FakeLrepConnectorLocalStorage.disableFakeConnector();
		// 	UIComponent.prototype.destroy.apply(this, arguments);

		// 	this._oMockServer.stop();
		// }
	});

});