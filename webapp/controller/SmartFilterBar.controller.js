sap.ui.define([
	'sap/m/MessageToast',
	"sap/ui/core/mvc/Controller"
], function(MessageToast, Controller) {
	"use strict";

	return Controller.extend("com.xiaomi.test.mismartchart.controller.SmartFilterBar", {
		oSmartFilterBar:null,
		oInteractiveBarChart:null,
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.xiaomi.test.mismartchart.view.SmartFilterBar
		 */
		onInit: function() {
			var oView = this.getView();
			var oModel = new sap.ui.model.json.JSONModel();
			oView.setModel(oModel, 'alpModel');
			oModel.setProperty('/filterHeaderOption', 'visual');
			oModel.setProperty('/exitFullscreenTable', false);
			oModel.setProperty('/contentView', 'charttable');
			oModel.setProperty('/exitFullscreenChart', false);
			oModel.setProperty('/showLegendAnalyticalChart', true);

			this.oSmartFilterBar = this.getView().byId("smartFilterBar");
			this.oInteractiveBarChart = this.getView().byId("interactiveBarChart");
			
			this.oInteractiveBarChart.getBinding("bars");
		},
		_onFioriAnalyticalListPageHeaderActionsSelectionChange: function(oEvent) {
			var oModel = this.getView().getModel('alpModel');
			var item = oEvent.getParameters();
			if (item && item.getKey) {
				var key = item.getKey();
				oModel.setProperty('/filterHeaderOption', key);
			}
		},
		_onFioriAnalyticalListPageHeaderActionsPress: function(oEvent) {
			var oModel = this.getView().getModel('alpModel');
			var key = oEvent.getSource().getProperty('key');
			oModel.setProperty('/filterHeaderOption', key);

		},
		_onFioriAnalyticalListPageHeaderActionsPress1: function(oEvent) {
			var oModel = this.getView().getModel('alpModel');
			var key = oEvent.getSource().getProperty('key');
			oModel.setProperty('/filterHeaderOption', key);

		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.xiaomi.test.mismartchart.view.SmartFilterBar
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.xiaomi.test.mismartchart.view.SmartFilterBar
		 */
		onAfterRendering: function() {
			var smartChart = this.getView().byId("smartChartGeneral");
			setTimeout(function afterTwoSeconds() {
				smartChart.getToolbar().removeAllContent();
			  smartChart.getChart().attachSelectData(function(oEvent) {
				var parameters = oEvent.getParameters();
				var selectedPoints = this.getSelectedDataPoints();
				console.log("SelectData" + selectedPoints.count);
				console.log(selectedPoints.dataPoints[selectedPoints.dataPoints.length-1].context.getObject());
			}).attachDeselectData(function(oEvent) {
				var selectedPoints = this.getSelectedDataPoints();
				console.log("DeselectData" + selectedPoints.count);
				console.log(selectedPoints.dataPoints);
			});
			}, 2000);
			
			
		},
		
		triggerRevenueValueHelp: function() {
			var oInput = this.oSmartFilterBar.getControlByKey("Product");
			oInput.fireValueHelpRequest.call(oInput);
		},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.xiaomi.test.mismartchart.view.SmartFilterBar
		 */
		//	onExit: function() {
		//
		//	}
		
		onFilterChange: function(oEvent) {
			var bars = this.oInteractiveBarChart.getBars();
			// var filter = oEvent.getSource().getFilterData()[oEvent.getParameters().getParameters().filterChangeReason];
			// console.log(filter);
			var filterData = oEvent.getSource().getFilterData();
			bars = bars.filter(function(bar) {
				return filterData.DeliveryCalendarQuarter.items.find(function(item) {
				  return item.key === bar.getLabel();
				});
			});
			this.oInteractiveBarChart.setSelectedBars(bars);
		},
		
		
		
		selectionChanged: function(oEvent) {
			var filterEntityProperty = oEvent.getSource().data("filterEntityProperty");
			var parameters = {};
			
			if(oEvent.getSource() instanceof sap.suite.ui.microchart.InteractiveLineChart) {
				parameters.selectedItem = oEvent.getParameters().point;
				parameters.selected = oEvent.getParameters().selected;
				parameters.selectedItems = oEvent.getParameters().selectedPoints;
			}else if (oEvent.getSource() instanceof sap.suite.ui.microchart.InteractiveBarChart) {
				parameters.selectedItem = oEvent.getParameters().bar;
				parameters.selected = oEvent.getParameters().selected;
				parameters.selectedItems = oEvent.getParameters().selectedBars;
			}

			var filterData = this.oSmartFilterBar.getFilterData();
			if(filterEntityProperty) {
				switch (filterEntityProperty) {
					case "DeliveryCalendarQuarter":
						filterData.DeliveryCalendarQuarter = filterData.DeliveryCalendarQuarter || {};
						filterData.DeliveryCalendarQuarter.items = filterData.DeliveryCalendarQuarter.items || [];
						if(parameters.selected) {
							filterData.DeliveryCalendarQuarter.items.push({
								key: parameters.selectedItem.getLabel(),
								text: parameters.selectedItem.getLabel()
							});
						}else {
							filterData.DeliveryCalendarQuarter.items = filterData.DeliveryCalendarQuarter.items.filter(function(item) {
								return parameters.selectedItem.getLabel() !== item.key;
							});	
						}
						break;
					case "":
					default:
					
				}
			}

			this.oSmartFilterBar.setFilterData(filterData, true);
			this.oSmartFilterBar.fireSearch();
		},
		
		selectionChangedStatus: function(oEvent) {
			var oBar = oEvent.getParameter("bar");
			var selectedBars = oEvent.getParameter("selectedBars");
			var bar = oEvent.getParameter("bar");
			var selected = oEvent.getParameter("selected");
			
			var filterData = this.oSmartFilterBar.getFilterData();
			var status = filterData.SalesOrderOverallStatus = filterData.SalesOrderOverallStatus || {};
			status.items = status.items || [];
			if(selected) {
				status.items.push({
					key: bar.getLabel(),
					text: bar.getLabel()
				});
			}else {
				status.items = status.items.filter(function(item) {
					return bar.getLabel() !== item.key;
				});	
			}
			
			this.oSmartFilterBar.setFilterData(filterData, true);
			MessageToast.show("The selection changed: " + oBar.getLabel() + " " + ((oBar.getSelected()) ? "selected" : "deselected") + " of " + selectedBars.length);
		},
		onGo: function() {
			this.oSmartFilterBar.fireSearch();
		},
		onSearch: function(oEvent) {
			// this.oSmartFilterBar.firePendingChange();
			var filters = this.oSmartFilterBar.getFilters();
			
			var interactiveBarChart = this.getView().byId("interactiveBarChart");
			var oBinding = interactiveBarChart.getBinding("bars");
			oBinding.filter(filters);
		},
		
		onChartChanged: function(oEvent) {
			var parameters = oEvent.getParameters();
		},
		
		onSelection: function(oEvent) {
			var parameters = oEvent.getParameters();
		},
		
		onPendingChange: function(oEvent) {
			var view = oEvent.getSource();
			var filters = view.getFilters();
			var model = this.getOwnerComponent().getModel();
			model.read("/SEPMRA_C_ALP_Product", {filters: filters});
			// model.refresh();
			// model.updateBindings(true);
			
			var interactiveBarChart = this.getView().byId("interactiveBarChart");
			var oBinding = interactiveBarChart.getBinding("bars");
			oBinding.refresh();
			// var oTemplate = interactiveBarChart.getBindingInfo("bars").template;
			// interactiveBarChart.unbindAggregation("bars");
			// interactiveBarChart.bindItems("/SEPMRA_C_ALP_Product/", oTemplate);
			
			
			// var interactiveBarChart = this.getView().byId("interactiveBarChart");
			// var oBinding = interactiveBarChart.getBinding("bars");
			// oBinding.filter(filters);
		}

	});

});